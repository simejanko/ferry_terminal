class Ferry:
    """ Class for all ferries. """

    def __init__(self, name, capacity, pricing):
        """
        :param name: ferry's name
        :param capacity: vehicle capacity of this ferry
        :param pricing: dict with vehicle types as keys (VehicleType enum) and prices as values.
                        Vehicles not included in a dict should not board this ferry.
        """
        self.name = name
        self.capacity = capacity
        self.pricing = pricing
        self.vehicles = []

    def __len__(self):
        return len(self.vehicles)

    def __str__(self):
        return "Ferry {}".format(self.name)

    def load(self, vehicle):
        """
        Loads vehicle on the ferry if the vehicle type is appropriate and it's not full yet.
        :param vehicle: vehicle to load
        """
        if vehicle.type in self.pricing and not self.is_full():
            self.vehicles.append(vehicle)
        else:
            raise ValueError

    def is_full(self):
        """ Returns whether this ferry is full. """
        return len(self) >= self.capacity

    def send_away(self):
        """ Ferry is sent away and returns empty. """
        self.vehicles = []
