class Employee:
    """ Class for tracking employee's earnings. """

    def __init__(self, cut=0.1):
        """
        :param cut: employee's cut from the tickets (0-1)
        """
        self.cut = cut
        self.income = 0

    def __str__(self):
        return "Employee with income {:.2f}".format(self.income)

    def take_cut(self, earnings):
        """ Gives employee his cut of the earnings. """
        self.income += self.cut * earnings
