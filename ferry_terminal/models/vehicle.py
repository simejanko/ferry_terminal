from enum import Enum
import random


class VehicleType(Enum):
    CAR = "Car"
    VAN = "Van"
    BUS = "Bus"
    TRUCK = "Truck"


class Vehicle:
    """ Class for all vehicles. """

    def __init__(self, gas, type):
        """
        :param gas: current amount of gas
        :param type: type of vehicle (VehicleType enum)
        """
        self.gas = gas
        self.type = type

    def fill_up(self):
        """ Fills up vehicle's gas. """
        self.gas = 1

    def __str__(self):
        return "{} with {:.2f} gas".format(self.type.value, self.gas)


class CargoVehicle(Vehicle):
    """ Class for vehicles with cargo doors. """

    def __init__(self, gas, type):
        """
        :param gas: current amount of gas
        :param type: type of vehicle (VehicleType enum)
        """
        assert type in {VehicleType.BUS, VehicleType.TRUCK}
        super().__init__(gas, type)
        self.cargo_doors_open = False

    def __str__(self):
        return "{} and {} cargo doors".format(super().__str__(), "open" if self.cargo_doors_open else "closed")


def generate_random_vehicle():
    """ Generates and returns random vehicle. """
    vehicle_class = Vehicle
    gas = random.random()
    type = random.choice(list(VehicleType))
    if type in {VehicleType.BUS, VehicleType.TRUCK}:
        vehicle_class = CargoVehicle

    return vehicle_class(gas, type)
