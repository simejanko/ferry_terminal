from ferry_terminal.models.employee import Employee
from ferry_terminal.models.ferry import Ferry
from ferry_terminal.models.vehicle import VehicleType
from ferry_terminal.models.vehicle import CargoVehicle
from ferry_terminal.models.vehicle import generate_random_vehicle

LOCATION_ARRIVAL = "Arrival"
LOCATION_GAS = "Gas station"
LOCATION_CUSTOMS = "Customs inspection"


class Terminal:
    """ CLI ferry terminal application. """

    def __init__(self):
        self.employee = Employee()

        small_ferry = Ferry('Small ferry', 8, {
            VehicleType.CAR: 3,
            VehicleType.VAN: 4,
        })
        large_ferry = Ferry('Large ferry', 6, {
            VehicleType.BUS: 5,
            VehicleType.TRUCK: 6,
        })
        self.ferries = [small_ferry, large_ferry]
        self.earnings = 0
        self.current_vehicle = None
        self.location = LOCATION_ARRIVAL
        print("Ferry terminal application. Ctrl-C to exit.")

    def status(self):
        """ Prints the current status and waits for user input to continue execution. """
        print("Earnings made: {}".format(self.earnings))
        print("Employee: {}".format(self.employee))

        if self.current_vehicle is not None:
            print("Current vehicle: {}".format(self.current_vehicle))
        else:
            print("No vehicle is being processed right now.")

        print("Location: {}".format(self.location))

        print('-' * 50)
        print()
        input("Press enter to continue")
        print()

    def accept_vehicle(self, vehicle):
        """ Accepts new vehicle and processes it. """
        self.current_vehicle = vehicle
        self.location = LOCATION_ARRIVAL
        self.status()

        self.check_gas()
        self.custom_inspection()
        self.load_vehicle()

    def check_gas(self):
        """ Checks gas of the current vehicle and refills it if necessary. """
        if self.current_vehicle.gas < 0.1:
            self.location = LOCATION_GAS
            self.current_vehicle.fill_up()

            self.status()

    def custom_inspection(self):
        """ Performs custom inspection on the current vehicle, if necessary. """
        if isinstance(self.current_vehicle, CargoVehicle):
            self.location = LOCATION_CUSTOMS
            self.current_vehicle.cargo_doors_open = True
            self.status()
            self.current_vehicle.cargo_doors_open = False
            self.status()

    def load_vehicle(self):
        """ Loads current vehicle on the appropriate ferry and charges the ticket. """
        for ferry in self.ferries:
            if self.current_vehicle.type in ferry.pricing:
                price = ferry.pricing[self.current_vehicle.type]
                self.earnings += price
                self.employee.take_cut(price)

                ferry.load(self.current_vehicle)
                self.location = ferry
                if ferry.is_full():
                    ferry.send_away()

                self.status()
                break
        else:
            print("No ferry found for this vehicle!")
            print()

if __name__ == '__main__':
    terminal = Terminal()
    while True:
        terminal.accept_vehicle(generate_random_vehicle())
